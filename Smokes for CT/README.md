# de_inferno for CT

## Smokes

### One way in apps

![ct_apps_oneway_01.jpg](img/ct_apps_oneway_01.jpg)

![ct_apps_oneway_02.jpg](img/ct_apps_oneway_02.jpg)

![ct_apps_oneway_03_result.jpg](img/ct_apps_oneway_03_result.jpg)

### Retake A - balcony smoke from long

![ct_retake_a_balcony_smoke_01.jpg](img/ct_retake_a_balcony_smoke_01.jpg)

![ct_retake_a_balcony_smoke_02.jpg](img/ct_retake_a_balcony_smoke_02.jpg)

![ct_retake_a_balcony_smoke_03.jpg](img/ct_retake_a_balcony_smoke_03.jpg)

### Retake A - short smoke from arch

![ct_retake_a_short_from_arch_01.jpg](img/ct_retake_a_short_from_arch_01.jpg)

![ct_retake_a_short_from_arch_02.jpg](img/ct_retake_a_short_from_arch_02.jpg)

![ct_retake_a_short_from_arch_03.jpg](img/ct_retake_a_short_from_arch_03.jpg)

### Retake A - balcony smoke from arch

![retake_a_balcony_from_arch_01.png](img/retake_a_balcony_from_arch_01.png)

![retake_a_balcony_from_arch_02.jpg](img/retake_a_balcony_from_arch_02.jpg)

![retake_a_balcony_from_arch_03.jpg](img/retake_a_balcony_from_arch_03.jpg)


### Deep banana from pool B site

![deep_banana_from_pool_01.jpg](img/deep_banana_from_pool_01.jpg)

![deep_banana_from_pool_02.png](img/deep_banana_from_pool_02.png)

![deep_banana_from_pool_03.jpg](img/deep_banana_from_pool_03.jpg)

![deep_banana_from_pool_04_result.jpg](img/deep_banana_from_pool_04_result.jpg)

### Retake B - Smoke off top banana

![retake_b_banana_from_ct_01.jpg](img/retake_b_banana_from_ct_01.jpg)

![retake_b_banana_from_ct_02.jpg](img/retake_b_banana_from_ct_02.jpg)

![retake_b_banana_from_ct_03_result.jpg](img/retake_b_banana_from_ct_03_result.jpg)

### Retake B - Smoke coffin from CT

![retake_b_coffin_from_ct_01.jpg](img/retake_b_coffin_from_ct_01.jpg)

![retake_b_coffin_from_ct_02.jpg](img/retake_b_coffin_from_ct_02.jpg)

![retake_b_coffin_from_ct_03.jpg](img/retake_b_coffin_from_ct_03.jpg)

![retake_b_coffin_from_ct_04_result.jpg](img/retake_b_coffin_from_ct_04_result.jpg)

### Retake B - Smoke off top banana

![retake_b_ct_banana_01.jpg](img/retake_b_ct_banana_01.jpg)

![retake_b_ct_banana_02.jpg](img/retake_b_ct_banana_02.jpg)

![retake_b_ct_banana_03.jpg](img/retake_b_ct_banana_03.jpg)

![retake_b_ct_banana_04_result.jpg](img/retake_b_ct_banana_04_result.jpg)

### Retake B - Smoke off "stairs" from CT spawn

![retake_b_stairs_from_ct_01.jpg](img/retake_b_stairs_from_ct_01.jpg)

![retake_b_stairs_from_ct_02.jpg](img/retake_b_stairs_from_ct_02.jpg)

![retake_b_stairs_from_ct_03.jpg](img/retake_b_stairs_from_ct_03.jpg)

![retake_b_stairs_from_ct_04_result.jpg](img/retake_b_stairs_from_ct_04_result.jpg)

