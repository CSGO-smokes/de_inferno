# de_inferno smokes

## Smokes


### Coffin smoke from banana

![coffin_banana_smoke_01.jpg](img/coffin_banana_smoke_01.jpg)

![coffin_banana_smoke_02.jpg](img/coffin_banana_smoke_02.jpg)

![coffin_banana_smoke_03.jpg](img/coffin_banana_smoke_03.jpg)

![coffin_banana_smoke_04_result.jpg](img/coffin_banana_smoke_04_result.jpg)


### Coffin smoke from logs

![coffin_logs_smoke_01.jpg](img/coffin_logs_smoke_01.jpg)

![coffin_logs_smoke_02.jpg](img/coffin_logs_smoke_02.jpg)

![coffin_logs_smoke_03.jpg](img/coffin_logs_smoke_03.jpg)


### CT (b) from T spawn

![ct_from_t_spawn_01.jpg](img/ct_from_t_spawn_01.jpg)

![ct_from_t_spawn_02.jpg](img/ct_from_t_spawn_02.jpg)

![ct_from_t_spawn_03_result.jpg](img/ct_from_t_spawn_03_result.jpg)


### CT (b) from logs in banana

![ct_logs_smoke_01 (1).jpg](img/ct_logs_smoke_01.jpg)

![ct_logs_smoke_02.jpg](img/ct_logs_smoke_02.jpg)

![ct_logs_smoke_03_result.jpg](img/ct_logs_smoke_03_result.jpg)


### CT (b) smoke from banana (pipe)

![ct_smoke_01.jpg](img/ct_smoke_01.jpg)

![ct_smoke_02.jpg](img/ct_smoke_02.jpg)

![ct_smoke_03.jpg](img/ct_smoke_03.jpg)


### CT (b) smoke from logs corner

![ct_smoke_alt02_01.jpg](img/ct_smoke_alt02_01.jpg)

![ct_smoke_alt02_02.jpg](img/ct_smoke_alt02_02.jpg)

![ct_smoke_alt02_03.jpg](img/ct_smoke_alt02_03.jpg)

![ct_smoke_alt02_04_result.jpg](img/ct_smoke_alt02_04_result.jpg)


### CT (b) smoke from broken

![ct_smoke_alt_01.jpg](img/ct_smoke_alt_01.jpg)

![ct_smoke_alt_02.jpg](img/ct_smoke_alt_02.jpg)

![ct_smoke_alt_03.jpg](img/ct_smoke_alt_03.jpg)

![ct_smoke_alt_04_result.jpg](img/ct_smoke_alt_04_result.jpg)

### CT (b) from newbox (AFTERPLANT)

This is an afterplant smoke

![newbox_ct_after_plant_01.jpg](img/newbox_ct_after_plant_01.jpg)

![newbox_ct_after_plant_02.jpg](img/newbox_ct_after_plant_02.jpg)

![newbox_ct_after_plant_03.jpg](img/newbox_ct_after_plant_03.jpg)

![newbox_ct_after_plant_04.jpg](img/newbox_ct_after_plant_04.jpg)

![newbox_ct_after_plant_05.jpg](img/newbox_ct_after_plant_05.jpg)

![newbox_ct_after_plant_06_result.jpg](img/newbox_ct_after_plant_06_result.jpg)


### Apartment balcony from second mid

![apps_balcony_from_second_mid_01.jpg](img/apps_balcony_from_second_mid_01.jpg)

![apps_balcony_from_second_mid_02.jpg](img/apps_balcony_from_second_mid_02.jpg)

![apps_balcony_from_second_mid_03_result.jpg](img/apps_balcony_from_second_mid_03_result.jpg)

![apps_balcony_from_second_mid_04_result.jpg](img/apps_balcony_from_second_mid_04_result.jpg)

### Balcony lurk smoke from apartments

![apps_lurk_smoke_01.jpg](img/apps_lurk_smoke_01.jpg)

![apps_lurk_smoke_02.jpg](img/apps_lurk_smoke_02.jpg)

![apps_lurk_smoke_03.jpg](img/apps_lurk_smoke_03.jpg)

### Pit smoke from apartments

![apps_pit_smoke_01.jpg](img/apps_pit_smoke_01.jpg)

![apps_pit_smoke_02.jpg](img/apps_pit_smoke_02.jpg)

![apps_pit_smoke_03_result.jpg](img/apps_pit_smoke_03_result.jpg)

![apps_pit_smoke_04_result.jpg](img/apps_pit_smoke_04_result.jpg)


### Library from second mid

![library_from_second_mid_01.jpg](img/library_from_second_mid_01.jpg)

![library_from_second_mid_02.jpg](img/library_from_second_mid_02.jpg)

![library_from_second_mid_03_result.jpg](img/library_from_second_mid_03_result.jpg)


### Long (a) smoke from balcony in second mid

![long_from_balcony_01.jpg](img/long_from_balcony_01.jpg)

![long_from_balcony_02.jpg](img/long_from_balcony_02.jpg)

![long_from_balcony_03_result.jpg](img/long_from_balcony_03_result.jpg)

### Long (a) from mid

![long_from_mid_01.jpg](img/long_from_mid_01.jpg)

![long_from_mid_02.jpg](img/long_from_mid_02.jpg)

![long_from_mid_03_result.jpg](img/long_from_mid_03_result.jpg)


### Long (a) from mid alternative

![long_from_mid_alt_01.jpg](img/long_from_mid_alt_01.jpg)

![long_from_mid_alt_02.jpg](img/long_from_mid_alt_02.jpg)

![long_from_mid_alt_03.jpg](img/long_from_mid_alt_03.jpg)


### Long (a) from second mid

![long_smoke_01.jpg](img/long_smoke_01.jpg)

![long_smoke_02.jpg](img/long_smoke_02.jpg)

![long_smoke_03.jpg](img/long_smoke_03.jpg)


### Long (a) from boiler

![long_from_boiler_01.jpg](img/long_from_boiler_01.jpg)

![long_from_boiler_02.jpg](img/long_from_boiler_02.jpg)

![long_from_boiler_03_result.jpg](img/long_from_boiler_03_result.jpg)


### Short (a) from boiler

![short_from_boiler_01.jpg](img/short_from_boiler_01.jpg)

![short_from_boiler_02.jpg](img/short_from_boiler_02.jpg)

![short_from_boiler_03_result.jpg](img/short_from_boiler_03_result.jpg)

### Short (a) from second mid
![short_smoke_01.jpg](img/short_smoke_01.jpg)

![short_smoke_02.jpg](img/short_smoke_02.jpg)

![short_smoke_03.jpg](img/short_smoke_03.jpg)

![short_smoke_04_result.jpg](img/short_smoke_04_result.jpg)




### Smoke off barrels on a site from mid (Good if you have short but not site)

![mid_to_a_site_hs_pos_01.jpg](img/mid_to_a_site_hs_pos_01.jpg)

![mid_to_a_site_hs_pos_02.jpg](img/mid_to_a_site_hs_pos_02.jpg)

![mid_to_a_site_hs_pos_03.jpg](img/mid_to_a_site_hs_pos_03.jpg)


### Motto from top middle

![mid_to_motto_01.jpg](img/mid_to_motto_01.jpg)

![mid_to_motto_02.jpg](img/mid_to_motto_02.jpg)

![mid_to_motto_03_result.jpg](img/mid_to_motto_03_result.jpg)

### Motto from apps

![motto_from_apps_01.jpg](img/motto_from_apps_01.jpg)

![motto_from_apps_02_result.jpg](img/motto_from_apps_02_result.jpg)


### Smoke off pit from second mid 

![pit_smoke_from_second_mid_01.jpg](img/pit_smoke_from_second_mid_01.jpg)

![pit_smoke_from_second_mid_02.jpg](img/pit_smoke_from_second_mid_02.jpg)

![pit_smoke_from_second_mid_03_result.jpg](img/pit_smoke_from_second_mid_03_result.jpg)

![pit_smoke_from_second_mid_04_result.jpg](img/pit_smoke_from_second_mid_04_result.jpg)


### Smoke off pit from second mid alternative

![pit_from_second_mid_alt_01.jpg](img/pit_from_second_mid_alt_01.jpg)

![pit_from_second_mid_alt_02.jpg](img/pit_from_second_mid_alt_02.jpg)

![pit_from_second_mid_alt_03.jpg](img/pit_from_second_mid_alt_03.jpg)



## Molotovs


### Molotov coffin from sandbags

Make sure CT is smoked

![molo_coffin_01.jpg](img/molo_coffin_01.jpg)

![molo_coffin_02.jpg](img/molo_coffin_02.jpg)

![molo_coffin_03_result.jpg](img/molo_coffin_03_result.jpg)


### Molotov coffin from broken

This is inconsistent, expect to miss sometimes

![molo_coffin_alt_01.jpg](img/molo_coffin_alt_01.jpg)

![molo_coffin_alt_02_result.jpg](img/molo_coffin_alt_02_result.jpg)


### Molotov newbox from car/banana

![molo_newbox_01.jpg](img/molo_newbox_01.jpg)

![molo_newbox_02.jpg](img/molo_newbox_02.jpg)

![molo_newbox_03_result.jpg](img/molo_newbox_03_result.jpg)


